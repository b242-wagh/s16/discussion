console.log("hello batch 242!!");

// javascript operators 

// arithmetic operator 

let x=1397;
let y=7831;

// sum 
let sum = x + y;
console.log("result of additoon operator is " + sum);

// differnce 
let diff = x - y;
console.log("result of subtraction operator is " + diff);

// multipliction 
let prod = x * y;
console.log("result of product  operator is " + prod);

// quotient
let div = x / y;
console.log("result of division operator is " + div);

// modulus 
let rem = x%y;
console.log("result of modulus operator is " + rem );

// assignment operators 
// basic assignmrnt operator (=)
let assignmentNumber = 8;

// addition assigment operator (+=)
assignmentNumber += 2;
console.log("result of additon assignemnt operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("result of additon assignemnt operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("result of suibtraction assignemnt operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("result of multiplication  assignemnt operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("result of division assignemnt operator: " + assignmentNumber);

// multiple operatores and parenthesis
let mdas = 1+2-3*4/5;
console.log("result of mdsa variable " + mdas);

// the order5 of poeration canbe changed by addng paranthesis to the logic
let pemdas = 1+(2-3)*(4/5);
console.log("result of pemdsa varibale " + pemdas);

// increment ansd decrement 
let z =1;

// pre-increment
let increment = ++z;
console.log("result of the pre-increment " + increment);
console.log("result of the pre-increment " + z);

// post increment
increment = z++;
console.log("result of the post-increment " + increment);
console.log("result of the post-increment " + z);

// pre-decrement
let decrement = --z;
console.log("result of the pre-decrement " + decrement);
console.log("result of the pre-decrement " + z);

// post decrement
decrement = z--;
console.log("result of the post-decrement " + decrement);
console.log("result of the post-decrement " + z);

// type coercion - the autoimatic or implicit conversion of values from 
// one data type to another 

let numA = '10';
let numB = 12;
let coersion = numA + numB;
console.log(coersion);
console.log(typeof coersion);

let numE = true +1;
console.log(numE);
console.log(typeof numE);

// comparison operators (==)
let juan = 'juan'; /* check for juan ='james' */
console.log("Equality operator");
console.log(1==1);
console.log(1=='1');
console.log(0 == false);
console.log('juan' =='juan');
console.log('juan' == juan);

// inequality operator(!=)
console.log("Inequality operator");
console.log(1!=1);
console.log(1!='1');
console.log(0!= false);
console.log('juan' !='juan');
console.log('juan' != juan);

// Strict equality operator (===)  this follows the pyton equality rule 
console.log("Strict Equality operator");
console.log(1===1);
console.log(1==='1');
console.log(0 === false);
console.log('juan' ==='juan');
console.log('juan' === juan);

// Strict equality operator (!==)  this follows the pyton equality rule 
console.log("Strict Inequality operator");
console.log(1!==1);
console.log(1!=='1');
console.log(0 !== false);
console.log('juan' !=='juan');
console.log('juan' !==juan);

// relational operator 
let a=50;
let b=65;

let isGreaterThan = a>b;
let isLessThan = a<b;
let isGTorEqual = a>=b;
let isLTorEqual = a<=b;

console.log("Relational operator")
console.log(isGreaterThan );
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

let numStr = '30';
console.log('relational operator with type coercion');
console.log(a>numStr);

// logical operators 
let is_legal_age = true;
let is_registered = false;

// And operator(&&)
//  return true if all aperands are true 
let all_requirements_met = is_legal_age && is_registered;
console.log("result of AND answer " + all_requirements_met);

// or operator(||)
let some_requirements_met = is_legal_age || is_registered;
console.log("result of OR answer " + some_requirements_met);

// not operator
let some_requirements_not_met = !is_registered;
console.log("result of NOT operator  " + some_requirements_not_met);




